# Import the Get-ComputerAdmins module

# get the current script dir
$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition

Import-Module ($scriptPath + "\Get-ComputerAdmins.psm1")

Get-ComputerAdmins

# Remove the module to cleanup the PowerShell environment
Remove-Module Get-ComputerAdmins