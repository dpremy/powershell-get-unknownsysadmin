Function GetKnownUser {
  write-host "  Getting list of known users..."
  $KnownUsers = @(
    "Domain/Domain-Admins",
    "Domain/myadmin",
    "$",
    "S-1-5-21-"
  )
  write-host "    Done...`n"
  return $KnownUsers
}

#Function to check if a user is running with Admin rights
function IsAdmin {
  write-host "  Check for users administrator status..."

  $wid=[System.Security.Principal.WindowsIdentity]::GetCurrent()
  $prp=new-object System.Security.Principal.WindowsPrincipal($wid)
  $admin=[System.Security.Principal.WindowsBuiltInRole]::Administrator

  if ($prp.IsInRole($admin)) {
    write-host "    User is an administrator on the system...`n"
  } else {
    write-host "    User is not an administrator on the system...`n"
  }

  return $prp.IsInRole($admin)
}

#Function to list all users within a given group
function ListGroupMembers ($CheckGroup) {
  write-host "    Checking users listed in" $CheckGroup "against known users...`n"

  $group =[ADSI]"WinNT://./$($CheckGroup)"
  $members = @($group.psbase.Invoke("Members"))
  return $members | foreach {$_.GetType().InvokeMember("ADSPath", 'GetProperty', $null, $_, $null)}
}

#Function to return all unknown users from a given list using the GetKnownUser
function UnknownUsers ($Group, $KnownUsers){
  $Unknowns = @();

  write-host "  Finding users listed in" $Group "..."

  ListGroupMembers($Group) | ForEach-Object {
    $unique = $true;
    foreach ($Known in $KnownUsers) {
      if ($_ -match [Regex]::Escape($Known)) {
        $unique = $false;
        break;
      }
    }

    if ($unique) {
      $Unknowns += $_.Replace("WinNT://", "");
      $UnknownUsersList = [System.Environment]::NewLine + [System.Environment]::NewLine + "User Group: " + $Group + [System.Environment]::NewLine + [System.String]::Join([System.Environment]::NewLine, $Unknowns)
    }
  }
  Return $UnknownUsersList
}

#Function to write the log of unknown users or remove logs that are no longer needed
Function LogComputer ($Log) {

  write-host "  Starting log process..."

  #Pull the environment variable ComputerName
  $Computer = get-content env:computername

  #Var to store the folder where log files will be created
  $LogFile = "\\fileserver\share$\AdminUsers\Logs\" + $Computer + ".log"

  #Check to see if a log even needs to be written
  if ($Log) {
    write-host "    Writting new log file to log directory...`n"

    #Add the current date to the log file and then write the file
    $Log = "Date run: " + $(Get-Date -format F) + [System.Environment]::NewLine + "Logged in user: " + [System.Security.Principal.WindowsIdentity]::GetCurrent().name + $Log

    #Write the log to the log file
    $Log=Foreach-Object {$Log -replace "\/", "\"}
    $Log | Out-File -force -encoding ASCII -filepath $LogFile
  } else {
    #Check if a log file exists and shouldn't
    if (Test-Path $LogFile) {
      write-host "    Deleting old log file...`n"

      #delete an old log file
      remove-item $LogFile
    } else {
      write-host "    No unknown administrative or power users found..."
      write-host "    No log files were created or deleted...`n"
    }
  }
}

Function Get-ComputerAdmins {
  <#
    .Synopsis
      Saves a log listing ever user which is an administrator on a computer.
    .Description
      This function will save a log for every user which is not in the known list of administrators who has admin rights on a computer. This can be used to help find computers which have unknown administrators.
    .Example
      Get-ComputerAdmins
    .Parameter -AdminOnly
      Run a full check only on users who are administrators.
    .Notes
      NAME:     Get-ComputerAdmins
      AUTHOR:   David P. Remy (Remy Services, LLC)
      LASTEDIT: 12/05/2016 v1.0.1
      LICENSE:  Distributed under the Microsoft Public License (MS-RL)
        This file is distributed under the Microsoft Reciprocal License.
        If you use the software, you accept this license.
        If you do not accept this license, do not use the software.
        See the LICENSE.txt file distributed with this code.
    .Link
      https://gitlab.com/dpremy/powershell-get-unknownsysadmin
  #>

  Param(
    [Parameter(Position=0, Mandatory=$false, HelpMessage = "Run a full check only on users who are administrators.")]
    [switch]$AdminOnly = $false)

  clear-host
  write-host "Audit of users is starting up...`n"

  if (($AdminOnly -and (IsAdmin)) -or (!($AdminOnly))) {
    $KnownUsers = GetKnownUser("")
    $UnknownUsers=""
    $UnknownUsers = UnknownUsers "Administrators" @($KnownUsers)
    $UnknownUsers += UnknownUsers "Power Users" @($KnownUsers)

    LogComputer($UnknownUsers)

    write-host "Audit has finished...`n"
  }
}

Export-ModuleMember Get-ComputerAdmins