@echo off
cls

title "Auditing Admin and Power users..."

copy /v /y "%~dp0\Get-ComputerAdmins.ps1" "%temp%\Get-ComputerAdmins.ps1" 1>nul 2>nul
copy /v /y "%~dp0\Get-ComputerAdmins.psm1" "%temp%\Get-ComputerAdmins.psm1" 1>nul 2>nul

powershell.exe -executionpolicy unrestricted "%temp%\Get-ComputerAdmins.ps1"

del /f /q "%temp%\Get-ComputerAdmins.ps1" 1>nul 2>nul
del /f /q "%temp%\Get-ComputerAdmins.psm1" 1>nul 2>nul